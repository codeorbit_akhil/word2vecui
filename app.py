from flask import Flask, render_template
import test as t
import urllib2

app = Flask(__name__)

@app.route("/")
def template_test():
    return render_template('search.html',title = "WORD2VEC") 

@app.route("/search/<query>")
def search(query = ""):
	# resDic = t.dic
	resDic = urllib2.urlopen("http://173.255.113.135:8000/search/item").read()
	resDic = eval(resDic)
	# print resDic
	result = {}
	for key, val in resDic.items():
		result.update({key:eval(val)})
	print result

	return render_template('result.html',result = result ,title = "Result")

if __name__ == '__main__':
    app.run(debug=True)
